package post

import (
	"fmt"
	"log"

	dbconn "gitlab.com/zendrulat123/books/db"
)

type Post struct {
	ID     string `json:"id" form:"id" query:"id"`
	Userid string `json:"userid" form:"userid" query:"userid"`
	TITLE  string `json:"title" form:"title" query:"title"`
	URL    string `json:"url" form:"url" query:"url"`
	HTML   string `json:"html" form:"html" query:"html"`
	CSS    string `json:"css" form:"css" query:"css"`
	JS     string `json:"js" form:"js" query:"js"`
}

//CreateUser creates a Post
func (p Post) CreatePost(ui string, t string, u string, h string, c string, j string) bool {
	//opening database
	data := dbconn.Conn()
	// query
	stmt, err := data.Prepare("INSERT INTO post(userid, title, url, html, css, js) VALUES(?,?,?,?)")
	if err != nil {
		log.Fatal(err)
	}
	//initializing Post
	posttemp := Post{Userid: ui, TITLE: t, URL: u, HTML: h, CSS: c, JS: j}
	//checking whats going into database because res below prints way too much info
	fmt.Println(posttemp, "- added to database")
	//add to the database
	res, err := stmt.Exec(posttemp.Userid, posttemp.TITLE, posttemp.URL, posttemp.HTML, posttemp.CSS, posttemp.JS)
	if err != nil {
		log.Fatal(err)
	}
	//if error then print first and last id
	lastId, err := res.LastInsertId()
	if err != nil {
		log.Fatal(err)
	}
	rowCnt, err := res.RowsAffected()
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("ID = %d, affected = %d\n", lastId, rowCnt)
	return true
}
