package main

import (
	"io"
	"net/http"
	"strconv"
	"strings"
	"sync"
	"text/template"
	"time"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	ed "gitlab.com/zendrulat123/books/handlers/processing/editorprocess"
	p "gitlab.com/zendrulat123/books/handlers/processing/processform"
	c "gitlab.com/zendrulat123/books/handlers/serving/contentedit"
	d "gitlab.com/zendrulat123/books/handlers/serving/dashboard"
	dp "gitlab.com/zendrulat123/books/handlers/serving/dashpages"
	f "gitlab.com/zendrulat123/books/handlers/serving/login"
)

/*database layout
database name is db
   user     | post | page
  -id       -id       -id
  -email    -html     -url
  -password -css      -title
		    -js        -content
*/

/*
/form->/dashboard->/contentedit
*/
func main() {

	e := echo.New()

	//routes
	//resources
	e.Use(middleware.Static("/public"))
	e.Static("/", "public")

	//middleware
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"http://localhost:1323", "http://localhost:1323/assets/", "http://96a156382e0d.ngrok.io/ping"},
		AllowMethods: []string{http.MethodGet, http.MethodPut, http.MethodPost, http.MethodDelete},
	}))
	//serving content
	e.GET("/contentedit/:userid", c.Contentedit)   //editor
	e.GET("/form", f.Login)                        //login form
	e.GET("/dashboard-home/:userid", d.Dashboard)  //dashboard after login
	e.GET("/dashboardpages/:userid", dp.Dashpages) //dashboard after login

	//services
	e.POST("/dashboard", p.Processingform)      //process user/show dash
	e.POST("/editor/:pageid", ed.Editorprocess) //process content editor

	//testing
	e.GET("/ping", func(c echo.Context) error {
		c.Render(http.StatusOK, "contentedit.html", map[string]interface{}{
			"User": "user",
		}) //Render
		return c.String(200, "test")
	})

	// Template serving
	renderer := &TemplateRenderer{
		templates: template.Must(template.ParseGlob("public/templates/*.html")),
	}
	e.Renderer = renderer
	e.Use(middleware.CORS())

	// Debug/middleware/templates
	e.Debug = true
	s := NewStats()
	e.Use(s.Process)
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())
	e.Use(middleware.LoggerWithConfig(middleware.LoggerConfig{
		Skipper: func(c echo.Context) bool {
			if strings.HasPrefix(c.Request().Host, "localhost") {
				return true
			}
			return false
		},
	}))
	e.Logger.Fatal(e.Start(":1323"))
}

type (
	Stats struct {
		Uptime       time.Time      `json:"uptime"`
		RequestCount uint64         `json:"requestCount"`
		Statuses     map[string]int `json:"statuses"`
		mutex        sync.RWMutex
	}
)

func NewStats() *Stats {
	return &Stats{
		Uptime:   time.Now(),
		Statuses: map[string]int{},
	}
}

// Process is the middleware function.
func (s *Stats) Process(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		if err := next(c); err != nil {
			c.Error(err)
		}
		s.mutex.Lock()
		defer s.mutex.Unlock()
		s.RequestCount++
		status := strconv.Itoa(c.Response().Status)
		s.Statuses[status]++
		return nil
	}
}

// Handle is the endpoint to get stats.
func (s *Stats) Handle(c echo.Context) error {
	s.mutex.RLock()
	defer s.mutex.RUnlock()
	return c.JSON(http.StatusOK, s)
}

// ServerHeader middleware adds a `Server` header to the response.
func ServerHeader(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		c.Response().Header().Set(echo.HeaderServer, "Echo/3.0")
		return next(c)
	}
}

// TemplateRenderer is a custom html/template renderer for Echo framework
type TemplateRenderer struct {
	templates *template.Template
}

// Render renders a template document
func (t *TemplateRenderer) Render(w io.Writer, name string, data interface{}, c echo.Context) error {

	// Add global methods if data is a map
	if viewContext, isMap := data.(map[string]interface{}); isMap {
		viewContext["reverse"] = c.Echo().Reverse
	}

	return t.templates.ExecuteTemplate(w, name, data)
}
