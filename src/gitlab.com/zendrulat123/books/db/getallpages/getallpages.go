package post

import (
	"fmt"

	dbconn "gitlab.com/zendrulat123/books/db"
	p "gitlab.com/zendrulat123/books/pkg/post"
)

//CreateUser creates a Post
func GetAllPosts() []p.Post {
	//opening database
	data := dbconn.Conn()

	var (
		id     string
		userid string
		title  string
		url    string
		post   []p.Post
	)
	i := 0
	//get from database
	rows, err := data.Query("select * from post")
	if err != nil {
		fmt.Println(err)
	}
	for rows.Next() {
		err := rows.Scan(&id, &userid, &title, &url)
		if err != nil {
			fmt.Println(err)
		} else {
			i++
			fmt.Println("scan ", i)
		}

		post = append(post, p.Post{ID: id, Userid: userid, TITLE: title, URL: url})

	}
	defer rows.Close()
	defer data.Close()
	return post
}
