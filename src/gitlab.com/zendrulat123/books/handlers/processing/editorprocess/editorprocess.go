package editor

import (
	"fmt"
	"net/http"

	"github.com/labstack/echo"
	p "gitlab.com/zendrulat123/books/pkg/post"
)

func Editorprocess(c echo.Context) error {
	//URL DATA
	// pageid := c.Param("pageid")
	// type URLPageid struct {
	// 	Pageid string
	// }
	// pageids := URLPageid{Pageid: pageid}
	//URL DATA
	userid := c.Param("userid")
	type URLUserid struct {
		Userid string
	}
	userids := URLUserid{Userid: userid}

	//FORM DATA
	title := c.FormValue("title")
	url := c.FormValue("url")
	html := c.FormValue("html")
	css := c.FormValue("css")
	js := c.FormValue("js")

	//initialize
	postcode := p.Post{Userid: userids.Userid, TITLE: title, URL: url, HTML: html, CSS: css, JS: js}

	//SAVE IN DATABASE
	check := p.Post.CreatePost(postcode, postcode.Userid, postcode.TITLE, postcode.URL, postcode.HTML, postcode.CSS, postcode.JS)
	if check != true {
		fmt.Println("post not created")
	}

	return c.Render(http.StatusOK, "dashboard.html", map[string]interface{}{
		"Page": postcode,
	}) //Render

}
