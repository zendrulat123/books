package dashpages

import (
	"net/http"

	"github.com/labstack/echo"
	allpages "gitlab.com/zendrulat123/books/db/getallpages"
)

func Dashpages(c echo.Context) error {
	userId := c.Param("userid")

	type URL struct {
		UserId string
	}
	u := URL{UserId: userId}

	type Page struct {
		PageId string
	}

	allpage := allpages.GetAllPosts()

	for _, thisuser := range allpage {
		if u.UserId == thisuser.Userid {
			p := Page{PageId: thisuser.ID}

			return c.Render(http.StatusOK, "dashpages.html", map[string]interface{}{
				"Page": p,
			}) //Render

		}
	}
	return c.Render(http.StatusOK, "dashpages.html", map[string]interface{}{
		"User": u,
	}) //Render

}
