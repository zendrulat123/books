package dashbord

import (
	"net/http"

	"github.com/labstack/echo"
)

func Dashboard(c echo.Context) error {
	userId := c.Param("userid")
	return c.Render(http.StatusOK, "dashboard.html", map[string]interface{}{
		"User": userId,
	}) //Render
}
